# openharmony_demo

### 介绍
openharmony: c++开发学习案例

### 软件架构
此工程是openharmony L2 c++开发学习demo。


### 安装教程

1.  将test/example放入源码test/目录下
2.  将third_party/mythird放入源码third_party/目录下
3.  子系统配置

配置文件路径：code-v3.0-LTS/OpenHarmony/build/subsystem_config.json

添加内容：
```
  "sub_example": {
    "project": "hmf/test",
    "path": "test/example",
    "name": "sub_example",
    "dir": "test"
  }
```

4.  组件配置

配置文件Hi3516DV300.json的路径：productdefine/common/products/Hi3516DV300.json

添加内容:
```
    "sub_example:partB":{},
    "sub_example:partA":{}
```

### 使用说明

#### 各种情况下的demo
1.  //test/example/partA/feature1

- 1.1 编译对象是一个静态库

2.  //test/example/partA/feature2
- 2.1. 编译结果是一个可执行文件，该执行文件调用：
- 2.1.1. 模块内静态库：example/partA/feature1
- 2.1.2. 跨模块动态库：example/partB/module

3.  //test/example/partA/feature2
- 3.1. 编译打包一个配置文件

4.  //test/example/partB/module
- 4.1. 编译一个动态库
- 4.2. example/partB/module/test下是对本动态库的一个googletest的测试模块

5.  //test/example/partB/gmock_test
- 5.1. 编译一个静态库。
- 5.2. /example/partB/gmock_test/test下是对本静态库的一个测试模块，主要使用gmock。

6.  //test/example/partB/dll_use
- 6.1. 是使用闭源动态库的demo

7.  //test/example/partB/mythird_use
- 7.1 第三方组件调用的demo

### 第三方组件demo
1.  //third_party/mythird
- 1.1 添加测试的第三方组件


### 编译
编译--build-target 后面跟要编译的模块名称，各模块名称都写在BUILD.gn中。

以example/partA/feature2为例，编译命令如下:
```
./build.sh --product-name Hi3516DV300 --ccache --build-target helloworld2_bin
```
### 运行
测试需要动态库放入开发板/system/lib目录中。
可执行程序放入自己的目录/data/test目录中，并修改成可执行权限。

修改系统权限，重新替换文件:
```
mount -o remount,rw /
```

依赖的动态库放入开发板/system/lib/的目录下:
```
hdc_std.exe file send Z:\out\ohos-arm-release\sub_example\partB\libmodule_lib.z.so /system/lib
```

可执行程序放入 开发板自建/data/test下：
```
hdc_std.exe file send Z:\out\ohos-arm-release\tests\unittest\sub_example\partB\helloworld2_bin /data/test

```

修改可执行权限：
```
chmod 0711 /data/test/helloworld2_bin
```

运行：
```
/data/test/helloworld2_bin
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技
