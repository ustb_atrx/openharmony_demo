#include <string>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>
#include <iostream>
using namespace std;
using namespace testing;

//定义需要模拟的接口类
class Student
{
public:
    virtual ~Student() {}
    virtual int getAge(string name) = 0;
    virtual std::string getNameString() = 0;
};

//模拟类
class MockStudent : public Student
{
public:
    MOCK_METHOD1(getAge, int(string name));
    MOCK_METHOD0(getNameString, string());
};

TEST(StudentGmockTest, testxx)
{
    //在这里Times(AtLeast(3))意思是调用至少3次，少1次会报出异常
    MockStudent XiaoMing;
    EXPECT_CALL(XiaoMing, getAge(_))
        .Times(AtLeast(3))
        .WillOnce(Return(18))
        .WillRepeatedly(Return(20));

    cout << "2020-02-03 : " << XiaoMing.getAge("XiaoMing") << endl;
    cout << "2020-02-04 : " << XiaoMing.getAge("XiaoMing") << endl;
    cout << "2020-02-05 : " << XiaoMing.getAge("XiaoMing") << endl;
    cout << "2020-02-06 : " << XiaoMing.getAge("XiaoMing") << endl;

    //在这里Times(1)意思是调用1次，少或着多都会报出异常
    string value =  "Hello World!";
    EXPECT_CALL(XiaoMing, getNameString())
        .Times(1)
        .WillOnce(Return(value));
    string returnValue1 = XiaoMing.getNameString();
    cout <<  "Returned1 Value: "  << returnValue1 << endl;
    string returnValue2 = XiaoMing.getNameString();
    cout <<  "Returned2 Value: "  << returnValue2 << endl;
}
