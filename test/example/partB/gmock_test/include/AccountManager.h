// AccountManager.h 
// the interface of external services which should be mocked 
#ifndef ACCOUNT_MANAGER_H
#define ACCOUNT_MANAGER_H

#include <string>

#include "Account.h"

class AccountManager
{
public:
    virtual ~AccountManager(){}
    virtual Account findAccountForUser(const std::string& userId) = 0;

    virtual void updateAccount(const Account& account) = 0;
};

#endif //ACCOUNT_MANAGER_H