/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <cstdio>
extern "C" {

#include "module.h"

}

using namespace testing::ext;


class ModuleBTest : public testing::Test {
public:
    static void SetUpTestCase();
    static void TearDownTestCase();
    void SetUp();
    void TearDown();
};

void ModuleBTest::SetUpTestCase() {}

void ModuleBTest::TearDownTestCase() {}

void ModuleBTest::SetUp()
{
    /**
     * @tc.setup: reset perfStat
     */
    printf("ModuleBTest::SetUp\n");
}

void ModuleBTest::TearDown() {
    printf("ModuleBTest::TearDown\n");
}

/**
 * @tc.name: ModuleBTest001
 * @tc.desc: Test bind start time and end
 * @tc.type: FUNC
 * @tc.require: AR000CUF6O
 */
HWTEST_F(ModuleBTest, ModuleBTest001, TestSize.Level0)
{
    // step 1:调用函数获取结果
    int actual = Sub(4, 1);

    // Step 2:使用断言比较预期与实际结果
    EXPECT_EQ(4, actual);
}

/**
 * @tc.name: ModuleBTest002
 * @tc.desc: invalid end time test
 * @tc.type: FUNC
 * @tc.require: AR000CUF6O
 */
HWTEST_F(ModuleBTest, ModuleBTest002, TestSize.Level0)
{
    EXPECT_EQ(12, Add(5, 7));
}
