#include "helloworld1.h"
#include "module.h"
#include <stdio.h>

void helloworld2(void)
{
    printf("[demo] hello world 2222\n");
#ifdef my_os
    printf("my_os defined in BUILD.gn\n");
#endif
#ifdef MY_DEFINE
    printf("hello defined MY_DEFINE\n");
#endif 
    helloworld1();
    module();
}

int main()
{
    helloworld2();
    return 0;
}